<?php

class MY_Model extends \CI_Model {

    /**
     * Nom de la table de la base de données attachée au modèle.
     * @var string
     */
    protected $table_name;

    /**
     * Clé primaire de la table de la base de données attachée au modèle.
     * @var string
     */
    protected $primary_key = 'id';

    /**
     * Forme sous laquelle sont retournés les jeux de résultats.
     * @var string
     */
    protected $return_type = 'array';

    public function __construct() {
        parent::__construct();
    }

    public function __call($name, array $arguments = NULL) {
        if (method_exists($this->db, $name)) {
            call_user_func_array(array($this->db, $name), $arguments);

            return $this;
        }
    }

    /**
     * Retourne une seule ligne de résultat par la clé primaire.
     *
     * @param mixed $id
     * @return mixed
     */
    public function find($id) {
        //SELECT * FROM {$this->table_name} WHERE {$this->primary_key} = {$id} 
        $query = $this->db->where($this->primary_key, $id)
                ->get($this->table_name);

        if (!isset($this->return_type) OR $this->return_type == 'array') {
            //Le résultat sera retourné sous forme de tableau.
            $row = $query->row_array();
        } else {
            //Le résultat sera retourné sous forme d'objet du type donné.
            $row = $query->row(0, $this->return_type);
        }

        return $row;
    }

    /**
     * Retourne un jeu de résultat.
     * 
     * @return mixed
     */
    public function find_all($start = FALSE, $limit = FALSE) {
        //SELECT * FROM {$this->table_name}
        $query = $this->db->get($this->table_name);

        if (!isset($this->return_type) OR $this->return_type == 'array') {
            //Les résultats seront retournés sous forme de tableau.
            $rows = $query->result_array();
        } else {
            //Les résultats seront retournés sous forme d'objets du type donné.
            $rows = $query->result($this->return_type);
        }

        return $rows;
    }

    /**
     * Insère une nouvelle ligne dans la base de données.
     * 
     * @param array $data
     * @return bool
     */
    public function insert(array $data) {
        //INSERT INTO {$this->table_name} VALUES ($data)
        $result = $this->db->insert($this->table_name, $data);

        return $result ? TRUE : FALSE;
    }

    /**
     * Met à jour une ligne de la base de données par sa clé primaire.
     * 
     * @param mixed $id
     * @param array $data
     * @return bool
     */
    public function update($id, array $data) {
        //UPDATE {$this->table_name} SET {$data} WHERE {$this->primary_key} = {$id}
        return $this->db->where($this->primary_key, $id)
                        ->update($this->table_name, $data);
    }

    /**
     * Supprime une ligne de la base de données par sa clé primaire.
     * 
     * @param type $id
     * @return bool
     */
    public function delete($id) {
        //DELETE * FROM {$this->table_name} WHERE {$this->primary_key} = {$id}
        return $this->db->where($this->primary_key, $id)
                        ->delete($this->table_name);
    }

    /**
     * Retourne le nombre total de lignes présentes dans la table.
     * 
     * @return int
     */
    public function record_count() {
        return $this->db->count_all($this->table_name);
    }

    /**
     * Setter de la propriété $return_type.
     * 
     * @param string $type
     * @return \MY_Model
     */
    public function return_as($type) {
        $this->return_type = $type;

        return $this;
    }

}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */