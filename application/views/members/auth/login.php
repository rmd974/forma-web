<!-- Formulaire de connexion -->
<div class="row" style="padding-top: 100px;">
    <div class="medium-4 medium-centered large-4 large-centered columns log-in-form">
        <form data-abide novalidate method="post" action="<?php echo site_url('auth/'); ?>">
            <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
            <div class="row column">
                <h4 class="text-center">Connexion</h4>   
                <?php include_once(APPPATH . '/views/partials/callouts/error.php'); ?>
                <label>Nom d'utilisateur
                    <input type="text" name="username" placeholder="Nom d'utilisateur" required />
                    <span class="form-error">Ce champ est obligatoire !</span>
                </label>
                <label>Mot de passe
                    <input type="password" name="password" placeholder="Mot de passe" required />
                    <span class="form-error">Ce champ est obligatoire !</span>
                </label>
                <button type="submit" class="button expanded">Connexion</button>
            </div>
        </form>
        <a href="<?php echo site_url('auth/register'); ?>">Pas de compte ? Inscrivez-vous !</a>
    </div>
</div>
<!-- /.Formulaire de connexion -->