<!-- Contenu principal -->
<div class="row" id="content">
    <div class="medium-10 columns">
        <h5>S'inscrire à la session n° <?php echo htmlspecialchars($session->id); ?></h5>
        <hr/>
        <p><strong>Domaine :</strong> <?php echo htmlspecialchars($session->domain->name); ?> </p>
        <p><strong>Formation :</strong> <?php echo htmlspecialchars($session->training->description); ?> </p>
        <p><strong>Date de début :</strong> <?php echo htmlspecialchars($session->start_date); ?> </p>
        <p><strong>Date de fin :</strong> <?php echo htmlspecialchars($session->end_date); ?> </p>
        <p><strong>Places disponibles :</strong> <?php echo htmlspecialchars($session->available_seats); ?> </p>
        <?php include_once(APPPATH . '/views/partials/callouts/success.php'); ?>
        <form data-abide novalidate method="post" action="<?php echo site_url('registrations/apply_to/' . $session->id); ?>">
            <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
            <input type="hidden" name="domain_id" value="<?php echo htmlspecialchars($session->domain->id); ?>" />
            <input type="hidden" name="training_id" value="<?php echo htmlspecialchars($session->training->id); ?>" />
            <input type="hidden" name="session_id" value="<?php echo htmlspecialchars($session->id); ?>" />
            <input type="hidden" name="participant_id" value="<?php echo htmlspecialchars($this->participant_model->user()->id); ?>" />
            <div class="row column">
                <input type="checkbox" id="cheque" name="cheque"><label for="cheque">Je paye par chèque</label>
                <span class="form-error">Ce champ est obligatoire !</span>
            </div>
            <button type="submit" id="form-submit" style="display: none;"></button>
            <button type="reset" id="form-reset" style="display: none;"></button>
        </form>
    </div>
    <div class="medium-2 columns">
        <h5>Actions</h5>
        <hr/>
        <div class="stacked button-group">
            <button class="button" onclick="document.getElementById('form-submit').click()">S'inscrire</button>
            <button class="button alert" onclick="document.getElementById('form-reset').click()">Réinitialiser</button>
        </div>
    </div>
</div>
