<?php echo doctype('html5'); ?>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!--Import de Foundation -->
        <link href="<?php echo base_url('assets/css/foundation.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <!-- Import du CSS global -->
        <link href="<?php echo base_url('assets/css/app.css'); ?>" rel="stylesheet" type="text/css"/>

        <?php
        //Ajout de css supplémentaire.
        if (!empty($css) AND count($css) > 0) {
            foreach ($css as $path) {
                echo '<link href="' . $path . '" rel="stylesheet" type="text/css"/>';
            }
        }
        ?>
        
        <title>Forma - <?php echo isset($title) ? $title : 'Page sans titre'; ?></title>
    </head>
    <body>