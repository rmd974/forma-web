<div class="top-bar">
    <div class="top-bar-left">
        <ul class="menu">
            <li class="menu-text">FORMA</li>
            <li><a href="<?php echo site_url('domains/index'); ?>">Domaines</a></li>
            <li><a href="<?php echo site_url('trainings/index'); ?>">Formations</a></li>
            <li><a href="<?php echo site_url('sessions/index'); ?>">Sessions</a></li>
            <li><a href="<?php echo site_url('registrations/index'); ?>">Inscriptions</a></li>
        </ul>
    </div>
    <div class="top-bar-right">
        <ul class="menu">
            <li>
                <?php
                if ($this->participant_model->is_logged_in()) {
                    echo '<a href="' . site_url('auth/logout') . '">' . htmlspecialchars($this->participant_model->user()->username) . ' (Déconnexion)</a>';
                }
                ?>
            </li>
        </ul>
    </div>
</div>

<br/>