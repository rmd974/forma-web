        <!--Import de jQuery -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/vendor/jquery.js'); ?>"></script>
        <!--Import de What-Input -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/vendor/what-input.js'); ?>"></script>
        <!--Import de Foundation.js -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/vendor/foundation.min.js'); ?>"></script>
        <!--Import de Font-Awesome -->
        <script src="https://use.fontawesome.com/c1c86dd085.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>