<?php if (count($this->session->flashdata('errors')) > 0) { ?>
    <div class="alert callout">
        <?php foreach ($this->session->flashdata('errors') as $error) { ?>
            <p><?php echo $error; ?></p>
        <?php } ?>
    </div>
<?php } ?>