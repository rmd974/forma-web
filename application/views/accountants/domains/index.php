<!-- Contenu principal -->
<div class="row" id="content">
    <div class="medium-10 columns">
        <h5>Liste des domaines</h5>
        <hr/>
        <?php include_once(APPPATH . '/views/partials/callouts/success.php'); ?>
        <?php if (count($domains) > 0) { ?>
            <!-- Liste des domaines -->
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nom</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = intval($offset);
                    foreach ($domains as $domain) {
                        ?>
                        <tr>
                            <td><?php echo ++$i; ?></td>
                            <td><?php echo htmlspecialchars($domain->name); ?></td>
                            <td>
                                <div class="small button-group">
                                    <a href="<?php echo site_url('domains/show/' . htmlspecialchars($domain->id)); ?>" class="button"><i class="fa fa-search"></i></a>
                                    <?php if ($this->participant_model->is_admin()) { ?>
                                        <a href="<?php echo site_url('domains/edit/' . htmlspecialchars($domain->id)); ?>" class="button secondary"><i class="fa fa-pencil-square-o"></i></a>
                                        <form method="post" action="<?php echo site_url('domains/destroy/' . htmlspecialchars($domain->id)); ?>" style="display: inline;">
                                            <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                                            <button type="submit" class="button alert"><i class="fa fa-trash"></i></button>
                                        <?php } ?>
                                    </form>
                                </div>
                            </td>                          
                        </tr>

                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <!-- /.Liste des domaines -->

            <hr/>

            <!-- /Pagination -->
            <?php echo $this->pagination->create_links(); ?>
            <!-- /.Pagination -->

        <?php } else { ?>
            <div class="callout primary">
                <h5>Information</h5>
                <p>Il n'y a pas de domaines pour le moment.</p>              
            </div>
        <?php } ?>
    </div>
    <?php if ($this->participant_model->is_admin()) { ?>
        <div class="medium-2 columns">
            <h5>Actions</h5>
            <hr/>
            <div class="stacked button-group">

                <a href="<?php echo site_url('domains/create'); ?>" class="button">Ajouter</a>

            </div>
        </div>
    <?php } ?>
</div>
