<!-- Contenu principal -->
<div class="row" id="content">
    <div class="medium-10 columns">
        <h5>Ajouter un domaine</h5>
        <hr/>
        <?php include_once(APPPATH . '/views/partials/callouts/success.php'); ?>
        <form data-abide novalidate method="post" action="<?php echo site_url('domains/create'); ?>">
            <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
            <div class="row column">
                <label>Nom
                    <input type="text" name="name" placeholder="Nom du domaine" required />
                    <span class="form-error">Ce champ est obligatoire !</span>
                </label>
            </div>
            <button type="submit" id="form-submit" style="display: none;"></button>
            <button type="reset" id="form-reset" style="display: none;"></button>
        </form>
    </div>
    <div class="medium-2 columns">
        <h5>Actions</h5>
        <hr/>
        <div class="stacked button-group">
            <button class="button" onclick="document.getElementById('form-submit').click()">Ajouter</button>
            <button class="button alert" onclick="document.getElementById('form-reset').click()">Réinitialiser</button>
        </div>
    </div>
</div>
