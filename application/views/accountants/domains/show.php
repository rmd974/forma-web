<!-- Contenu principal -->
<div class="row" id="content">
    <div class="medium-10 columns">
        <h5>Affichage du domaine <?php echo htmlspecialchars($domain->name); ?></h5>
        <hr/>
        <?php if (count($domain->trainings) > 0) { ?>
            <p>Liste des formations appartenant à ce domaine : </p>
            <!-- Liste des formations appartenant au domaine -->
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Description</th>
                        <th>Lieu</th>
                        <th>Coût</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = intval($offset);
                    foreach ($domain->trainings as $training) {
                        ?>
                        <tr>
                            <td><?php echo ++$i; ?></td>
                            <td><?php echo character_limiter(htmlspecialchars($training->description), 50); ?></td>
                            <td><?php echo character_limiter(htmlspecialchars($training->place), 20); ?></td>
                            <td><?php echo htmlspecialchars($training->cost); ?></td>
                            <td>
                                <div class="small button-group">
                                    <a href="<?php echo site_url('trainings/show/' . htmlspecialchars($training->id)); ?>" class="button"><i class="fa fa-search"></i></a>
                                    <?php if ($this->participant_model->is_admin()) { ?>
                                        <form method="post" action="<?php echo site_url('trainings/destroy/' . htmlspecialchars($training->id)); ?>" style="display: inline;">
                                            <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                                            <button type="submit" class="button alert"><i class="fa fa-trash"></i></button>
                                        </form>
                                    <?php } ?>
                                </div>
                            </td>                          
                        </tr>

                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <!-- /.Liste des formations appartenant au domaine -->

            <hr/>

            <!-- /Pagination -->
            <?php echo $this->pagination->create_links(); ?>
            <!-- /.Pagination -->

        <?php } else { ?>
            <div class="callout primary">
                <h5>Information</h5>
                <p>Aucune formation n'est associée à ce domaine pour le moment.</p>   
                <?php if ($this->participant_model->is_admin()) { ?>
                    <a href="<?php echo site_url('trainings/create'); ?>" class="button">Ajouter</a>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
    <?php if ($this->participant_model->is_admin()) { ?>
        <div class="medium-2 columns">
            <h5>Actions</h5>
            <hr/>
            <div class="stacked button-group">
                <form method="post" action="<?php echo site_url('domains/destroy/' . htmlspecialchars($domain->id)); ?>" style="display: inline;">
                    <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                    <button type="submit" class="button alert">Supprimer</button>
                </form>
            </div>
        </div>
    <?php } ?>
</div>