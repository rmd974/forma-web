<!-- Contenu principal -->
<div class="row" id="content">
    <div class="medium-10 columns">
        <h5>Liste des sessions</h5>
        <hr/>
        <?php include_once(APPPATH . '/views/partials/callouts/success.php'); ?>
        <?php if (count($sessions) > 0) { ?>
            <!-- Liste des sessions -->
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Domaine</th>
                        <th>Formation</th>
                        <th>Date de début</th>
                        <th>Date de fin</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = intval($offset);
                    foreach ($sessions as $session) {
                        ?>
                        <tr>
                            <td><?php echo ++$i; ?></td>
                            <td><?php echo htmlspecialchars($session->domain->name); ?></td>
                            <td>Formation n°<?php echo htmlspecialchars($session->training->id); ?></td>
                            <td><?php echo htmlspecialchars($session->start_date); ?></td>
                            <td><?php echo htmlspecialchars($session->end_date); ?></td>
                            <td>
                                <div class="small button-group">
                                    <a href="<?php echo site_url('sessions/show/' . htmlspecialchars($session->id)); ?>" class="button"><i class="fa fa-search"></i></a>
                                    <?php if ($this->participant_model->is_admin()) { ?>
                                        <a href="<?php echo site_url('sessions/edit/' . htmlspecialchars($session->id)); ?>" class="button secondary"><i class="fa fa-pencil-square-o"></i></a>
                                        <form method="post" action="<?php echo site_url('sessions/destroy/' . htmlspecialchars($session->id)); ?>" style="display: inline;">
                                            <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                                            <button type="submit" class="button alert"><i class="fa fa-trash"></i></button>
                                        </form>
                                    <?php } else { ?>
                                        <a href="<?php echo site_url('registrations/register/' . htmlspecialchars($session->id)); ?>" class="button success"><i class="fa fa-wpforms"></i></a>
                                    <?php } ?>
                                </div>
                            </td>                          
                        </tr>

                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <!-- /.Liste des sessions -->

            <hr/>

            <!-- /Pagination -->
            <?php echo $this->pagination->create_links(); ?>
            <!-- /.Pagination -->

        <?php } else { ?>
            <div class="callout primary">
                <h5>Information</h5>
                <p>Il n'y a pas de sessions pour le moment.</p>              
            </div>
        <?php } ?>
    </div>
    <?php if ($this->participant_model->is_admin()) { ?>
        <div class="medium-2 columns">
            <h5>Actions</h5>
            <hr/>
            <div class="stacked button-group">
                <a href="<?php echo site_url('sessions/create'); ?>" class="button">Ajouter</a>
                <a href="" class="button alert disabled">Supprimer</a>
            </div>
        </div>
    <?php } ?>
</div>
