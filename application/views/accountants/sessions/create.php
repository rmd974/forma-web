<!-- Contenu principal -->
<div class="row" id="content">
    <div class="medium-10 columns">
        <h5>Ajouter une session</h5>
        <hr/>
        <?php include_once(APPPATH . '/views/partials/callouts/error.php'); ?>
        <?php include_once(APPPATH . '/views/partials/callouts/success.php'); ?>
        <form data-abide novalidate method="post" action="<?php echo site_url('sessions/create'); ?>">
            <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
            <div class="row column">  
                <label>Formation
                    <select name="training_id" required >
                        <?php
                        foreach ($trainings as $training) {
                            echo '<option value="' . htmlspecialchars($training->id) . '">' . htmlspecialchars($training->description) . '</option>';
                        }
                        ?>
                    </select>
                    <span class="form-error">Ce champ est obligatoire !</span>
                </label>
                <label>Date de début
                    <input type="text" name="start_date" placeholder="Saisissez une date de début de la session." required />
                    <span class="form-error">Ce champ est obligatoire !</span>
                </label>
                <label>Date de fin
                    <input type="text" name="end_date" placeholder="Saisissez une date de fin de la session." required />
                    <span class="form-error">Ce champ est obligatoire !</span>
                </label>
                <label>Description
                    <textarea name="description" placeholder="Saisissez la description de la session." required ></textarea> 
                    <span class="form-error">Ce champ est obligatoire !</span>
                </label>
                <label>Places disponibles
                    <input type="text" name="available_seats" placeholder="Saisissez le nombre de places disponibles de la session." required />
                    <span class="form-error">Ce champ est obligatoire !</span>
                </label>
                <button type="submit" id="form-submit" style="display: none;"></button>
                <button type="reset" id="form-reset" style="display: none;"></button>
            </div>
        </form>
    </div>
    <div class="medium-2 columns">
        <h5>Actions</h5>
        <hr/>
        <div class="stacked button-group">
            <button class="button" onclick="document.getElementById('form-submit').click()">Ajouter</button>
            <button class="button alert" onclick="document.getElementById('form-reset').click()">Réinitialiser</button>
        </div>
    </div>
</div>


