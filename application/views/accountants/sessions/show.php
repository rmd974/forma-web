<!-- Contenu principal -->
<div class="row" id="content">
    <div class="medium-10 columns">
        <h5>Affichage de la session n° <?php echo htmlspecialchars($session->id); ?></h5>
        <hr/>
        <p><strong>Domaine :</strong> <?php echo htmlspecialchars($session->domain->name); ?> </p>
        <p><strong>Formation :</strong> <?php echo htmlspecialchars($session->training->description); ?> </p>
        <p><strong>Date de début :</strong> <?php echo htmlspecialchars($session->start_date); ?> </p>
        <p><strong>Date de fin :</strong> <?php echo htmlspecialchars($session->end_date); ?> </p>
        <p><strong>Places disponibles :</strong> <?php echo htmlspecialchars($session->available_seats); ?> </p>
    </div>
    <div class="medium-2 columns">
        <h5>Actions</h5>
        <hr/>
        <div class="stacked button-group">
            <?php if ($this->participant_model->is_admin()) { ?>
                <a href="<?php echo site_url('sessions/edit/' . htmlspecialchars($session->id)); ?>" class="button secondary">Modifier</a>
                <form method="post" action="<?php echo site_url('sessions/destroy/' . htmlspecialchars($session->id)); ?>" style="display: inline;">
                    <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                    <button type="submit" class="button alert">Supprimer</button>
                </form>
            <?php } else { ?>
                <a href="<?php echo site_url('registrations/register/' . htmlspecialchars($session->id)); ?>" class="button success">S'inscrire</a>
            <?php } ?>
        </div>
    </div>
</div>