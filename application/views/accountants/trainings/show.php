<!-- Contenu principal -->
<div class="row" id="content">
    <div class="medium-10 columns">
        <h5>Affichage de la formation n° <?php echo htmlspecialchars($training->id); ?></h5>
        <hr/>
        <p><strong>Description :</strong> <?php echo htmlspecialchars($training->description); ?> </p>
        <p><strong>Lieu :</strong> <?php echo htmlspecialchars($training->place); ?> </p>
        <p><strong>Intervenant :</strong> <?php echo htmlspecialchars($training->speaker); ?> </p>
        <p><strong>Objectifs :</strong> <?php echo htmlspecialchars($training->objectives); ?> </p>
        <p><strong>Public :</strong> <?php echo htmlspecialchars($training->audience); ?> </p>
        <p><strong>Organisation :</strong> <?php echo htmlspecialchars($training->scheduling); ?> </p>
        <p><strong>Coût :</strong> <?php echo htmlspecialchars($training->cost); ?> € </p>
        <hr/>
        <?php if (count($training->sessions) > 0) { ?>
            <p>Liste des sessions appartenant à cette formation : </p>
            <!-- Liste des sessions associées à la formation -->
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Date de début</th>
                        <th>Date de fin</th>
                        <th>Places disponibles</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = intval($offset);
                    foreach ($training->sessions as $session) {
                        ?>
                        <tr>
                            <td><?php echo ++$i; ?></td>
                            <td><?php echo htmlspecialchars($session->start_date); ?></td>
                            <td><?php echo htmlspecialchars($session->end_date); ?></td>
                            <td><?php echo htmlspecialchars($session->available_seats); ?></td>
                            <td>
                                <div class="small button-group">
                                    <a href="<?php echo site_url('sessions/show/' . htmlspecialchars($session->id)); ?>" class="button"><i class="fa fa-search"></i></a>
                                    <?php if ($this->participant_model->is_admin()) { ?>
                                        <a href="<?php echo site_url('sessions/edit/' . htmlspecialchars($session->id)); ?>" class="button secondary"><i class="fa fa-pencil-square-o"></i></a>
                                        <form method="post" action="<?php echo site_url('sessions/destroy/' . htmlspecialchars($session->id)); ?>" style="display: inline;">
                                            <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                                            <button type="submit" class="button alert"><i class="fa fa-trash"></i></button>
                                        </form>
                                    <?php } ?>
                                </div>
                            </td>                          
                        </tr>

                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <!-- /.Liste des sessions associées à la formation -->

            <hr/>

            <!-- /Pagination -->
            <?php echo $this->pagination->create_links(); ?>
            <!-- /.Pagination -->

        <?php } else { ?>
            <div class="callout primary">
                <h5>Information</h5>
                <p>Il n'y a pas de sessions associées à cette formation pour le moment.</p>              
            </div>
        <?php } ?>
    </div>
    <?php if ($this->participant_model->is_admin()) { ?>
        <div class="medium-2 columns">
            <h5>Actions</h5>
            <hr/>
            <div class="stacked button-group">
                <a href="<?php echo site_url('trainings/edit/' . htmlspecialchars($training->id)); ?>" class="button secondary">Modifier</a>
                <form method="post" action="<?php echo site_url('trainings/destroy/' . htmlspecialchars($training->id)); ?>" style="display: inline;">
                    <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                    <button type="submit" class="button alert">Supprimer</button>
                </form>
            </div>
        </div>
    <?php } ?>
</div>