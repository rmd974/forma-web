<!-- Contenu principal -->
<div class="row" id="content">
    <div class="medium-10 columns">
        <h5>Liste des formations</h5>
        <hr/>
        <?php if (count($trainings) > 0) { ?>
            <!-- Liste des formations -->
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Domaine</th>
                        <th>Description</th>
                        <th>Coût</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = intval($offset);
                    foreach ($trainings as $training) {
                        ?>
                        <tr>
                            <td><?php echo ++$i; ?></td>
                            <td><?php echo htmlspecialchars($training->domain->name); ?></td>
                            <td><?php echo character_limiter(htmlspecialchars($training->description), 30); ?></td>
                            <td><?php echo htmlspecialchars($training->cost); ?></td>
                            <td>
                                <div class="small button-group">
                                    <a href="<?php echo site_url('trainings/show/' . htmlspecialchars($training->id)); ?>" class="button"><i class="fa fa-search"></i></a>
                                    <?php if ($this->participant_model->is_admin()) { ?>
                                        <a href="<?php echo site_url('trainings/edit/' . htmlspecialchars($training->id)); ?>" class="button secondary"><i class="fa fa-pencil-square-o"></i></a>
                                        <form method="post" action="<?php echo site_url('trainings/destroy/' . htmlspecialchars($training->id)); ?>" style="display: inline;">
                                            <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                                            <button type="submit" class="button alert"><i class="fa fa-trash"></i></button>                               
                                        </form>
                                    <?php } ?>
                                </div>
                            </td>                          
                        </tr>

                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <!-- /.Liste des formations -->

            <hr/>

            <!-- /Pagination -->
            <?php echo $this->pagination->create_links(); ?>
            <!-- /.Pagination -->

        <?php } else { ?>
            <div class="callout primary">
                <h5>Information</h5>
                <p>Il n'y a pas de formations pour le moment.</p>              
            </div>
        <?php } ?>
    </div>
    <?php if ($this->participant_model->is_admin()) { ?>
        <div class="medium-2 columns">
            <h5>Actions</h5>
            <hr/>
            <div class="stacked button-group">
                <a href="<?php echo site_url('trainings/create'); ?>" class="button">Ajouter</a>
                <a href="" class="button alert disabled">Supprimer</a>
            </div>
        </div>
    <?php } ?>
</div>
