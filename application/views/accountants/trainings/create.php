<!-- Contenu principal -->
<div class="row" id="content">
    <div class="medium-10 columns">
        <h5>Ajouter une formation</h5>
        <hr/>
        <?php include_once(APPPATH . '/views/partials/callouts/error.php'); ?>
        <?php include_once(APPPATH . '/views/partials/callouts/success.php'); ?>
        <form data-abide novalidate method="post" action="<?php echo site_url('trainings/create'); ?>">
            <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
            <div class="row column">  
                <label>Domaine
                    <select name="domain_id" required >
                        <?php
                        foreach ($domains as $domain) {
                            echo '<option value="' . htmlspecialchars($domain->id) . '">' . htmlspecialchars($domain->name) . '</option>';
                        }
                        ?>
                    </select>
                    <span class="form-error">Ce champ est obligatoire !</span>
                </label>
                <label>Description
                    <textarea name="description" placeholder="Saisissez une description de la formation." required ></textarea> 
                    <span class="form-error">Ce champ est obligatoire !</span>
                </label>
                <label>Lieu
                    <input type="text" name="place" placeholder="Saisissez le lieu où se déroule la formation." required />
                    <span class="form-error">Ce champ est obligatoire !</span>
                </label>
                <label>Intervenant
                    <input type="text" name="speaker" placeholder="Saisissez l'intervenant qui assistera à la formation." required />
                    <span class="form-error">Ce champ est obligatoire !</span>
                </label>
                <label>Objectifs
                    <textarea name="objectives" placeholder="Saisissez les objectifs de la formation." required ></textarea> 
                    <span class="form-error">Ce champ est obligatoire !</span>
                </label>
                <label>Public
                    <input type="text" name="audience" placeholder="Saisissez le public visé par la formation." required />
                    <span class="form-error">Ce champ est obligatoire !</span>
                </label>
                <label>Organisation
                    <input type="text" name="scheduling" placeholder="Saisissez l'organisation de la formation." required />
                    <span class="form-error">Ce champ est obligatoire !</span>
                </label>
                <label>Coût
                    <input type="text" name="cost" placeholder="Saisissez le coût de la formation." required />
                    <span class="form-error">Ce champ est obligatoire !</span>
                </label>
                <button type="submit" id="form-submit" style="display: none;"></button>
                <button type="reset" id="form-reset" style="display: none;"></button>
            </div>
        </form>
    </div>
    <div class="medium-2 columns">
        <h5>Actions</h5>
        <hr/>
        <div class="stacked button-group">
            <button class="button" onclick="document.getElementById('form-submit').click()">Ajouter</button>
            <button class="button alert" onclick="document.getElementById('form-reset').click()">Réinitialiser</button>
        </div>
    </div>
</div>


