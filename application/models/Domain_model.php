<?php

class Domain_model extends MY_Model {

    /**
     * Nom de la table de la base de données associée au modèle.
     * @var tring 
     */
    protected $table_name = 'domain';

    /**
     * Forme sous laquelle sont retournés les jeux de résultats.
     * @var string
     */
    protected $return_type = 'Domain';

    public function __construct() {
        parent::__construct();
        $this->load->library('Domain');
    }

}

/* End of file Domain_model.php */
/* Location: ./application/models/Domain_model.php */