<?php

class Session_model extends MY_Model {

    /**
     * Nom de la table de la base de données associée au modèle.
     * @var tring 
     */
    protected $table_name = 'session';

    /**
     * Forme sous laquelle sont retournés les jeux de résultats.
     * @var string
     */
    protected $return_type = '_Session';

    public function __construct() {
        parent::__construct();
        $this->load->library('_Session');
    }

}

/* End of file Session_model.php */
/* Location: ./application/models/Session_model.php */