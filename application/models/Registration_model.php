<?php

class Registration_model extends MY_Model {

    /**
     * Nom de la table de la base de données associée au modèle.
     * @var tring 
     */
    protected $table_name = 'registration';

    /**
     * Forme sous laquelle sont retournés les jeux de résultats.
     * @var string
     */
    protected $return_type = 'Registration';

    public function __construct() {
        parent::__construct();
        $this->load->library('Registration');
    }

}

/* End of file Registration_model.php */
/* Location: ./application/models/Registration_model.php */