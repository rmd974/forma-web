<?php

class Participant_model extends MY_Model {

    /**
     * Nom de la table de la base de données associée au modèle.
     * @var tring 
     */
    protected $table_name = 'participant';

    /**
     * Forme sous laquelle sont retournés les jeux de résultats.
     * @var string
     */
    protected $return_type = 'Participant';

    public function __construct() {
        parent::__construct();

        //Chargement de l'objet Participant.
        $this->load->library('Participant');
        //Chargement de la bibliothèque des sessions.
        $this->load->library('session');
    }

    /**
     * Connecte un participant en vérifiant la combinaison nom d'utilisateur/mot de passe.
     * 
     * @param string $username
     * @param string $password
     * @return bool
     */
    public function login($username, $password) {
        //SELECT * FROM {$this->table_name} WHERE username = {username}
        $query = $this->db->where('username', $username)
                ->get($this->table_name);

        //On retouurne un objet de type participant.
        $participant = $query->row(0, $this->return_type);

        //Si il y a un résultat et que les mots de passe correspondent on continue.
        if (!empty($participant) AND $this->_check_password($password, $participant->password)) {
            //On stocke l'objet utilisateur en session.
            $this->session->set_userdata(array('user' => serialize($participant)));
            return TRUE;
        }

        //FALSE sinon.
        return FALSE;
    }

    public function register($username, $password, array $data = array()) {
        //Données à insérer dans la base de données.
        $r_data = array(
            'username' => $username,
            'password' => password_hash($password, PASSWORD_DEFAULT)
        );

        $r_data = array_merge($r_data, $data);

        return $this->insert($r_data);
    }

    /**
     * Déconnecte l'utilisateur.
     * 
     * @return void
     */
    public function logout() {
        $this->session->unset_userdata('user');
    }

    /**
     * Retourne l'objet de l'utilisateur courant.
     * 
     * @return Participant
     */
    public function user() {
        return $this->is_logged_in() ? unserialize($this->session->userdata('user')) : NULL;
    }

    /**
     * Retourne TRUE si l'utilisateur est connecté, FALSE sinon.
     * 
     * @return bool
     */
    public function is_logged_in() {
        return $this->session->has_userdata('user');
    }

    /**
     * Retourne TRUE si l'utilisateur est administrateur (comptable), FALSE sinon.
     * 
     * @return bool
     */
    public function is_admin() {
        return (bool) $this->user()->is_admin;
    }

    /**
     * Compare un mot de passe avec son hash correspondant.
     * 
     * @param string $password
     * @param string $hash
     * @return bool
     */
    private function _check_password($password, $hash) {
        return password_verify($password, $hash);
    }

}
