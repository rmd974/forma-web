<?php

class Training_model extends MY_Model {

    /**
     * Nom de la table de la base de données associée au modèle.
     * @var tring 
     */
    protected $table_name = 'training';

    /**
     * Forme sous laquelle sont retournés les jeux de résultats.
     * @var string
     */
    protected $return_type = 'Training';

    public function __construct() {
        parent::__construct();
        $this->load->library('Training');
    }

}

/* End of file Training_model.php */
/* Location: ./application/models/Training_model.php */