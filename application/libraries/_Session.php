<?php

class _Session {

    public $domain_id;
    public $training_id;
    public $id;
    public $status;
    public $description;
    public $available_seats;
    public $start_date;
    public $end_date;

}

/* End of file Session.php */
/* Location: ./application/libraries/Session.php */