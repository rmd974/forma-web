<?php

namespace App\Libraries;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Association {

    public $icom_number;
    public $name;

}

/* End of file Association.php */
/* Location: ./application/libraries/Association.php */