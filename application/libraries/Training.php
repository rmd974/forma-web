<?php

class Training {

    public $domain_id;
    public $id;
    public $name;
    public $description;
    public $place;
    public $speaker;
    public $objectives;
    public $audience;
    public $cost;
    public $scheduling;

}

/* End of file Training.php */
/* Location: ./application/libraries/Training.php */