<?php

class Participant {

    public $id;
    public $association_icom_number;
    public $username;
    public $password;
    public $last_name;
    public $first_name;
    public $job;
    public $status;
    public $is_admin;

}

/* End of file Participant.php */
/* Location: ./application/libraries/Participant.php */