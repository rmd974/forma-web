<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Registrations extends MY_Controller {

    public function __construct() {
        parent::__construct();

        //Vérification que l'utilisateur est bien connecté.
        if (!$this->participant_model->is_logged_in()) {
            redirect('auth/index');
        }

        //Chargement de la bibliothèque de validation de formulaire.
        $this->load->library('form_validation');
        //Chargement du helper de formulaires.
        $this->load->helper('form');
        //Importation du modèle Registration.
        $this->load->model('registration_model');
        //Importation du modèle Session.
        $this->load->model('session_model');
    }

    /**
     * Gère l'inscription à une session.
     * 
     * @param int $id
     */
    public function apply_to($id = '') {
        if (!is_file(APPPATH . '/views/partials/header.php') OR ! is_file(APPPATH . '/views/partials/navbar.php')
                OR ! is_file(APPPATH . '/views/members/registrations/apply.php') OR ! is_file(APPPATH . '/views/partials/footer.php') OR empty($id) OR ! ( $session = $this->session_model->find($id))) {
            show_404();
        }

        //Jeton de sécurité.
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        //Importation du modèle Domain.
        $this->load->model('domain_model');
        //Importation du modèle Training.
        $this->load->model('training_model');

        //SELECT * FROM domain WHERE id = {$session->domain_id}.
        $session->domain = $this->domain_model->find($session->domain_id);

        //SELECT * FROM training WHERE id = {$session->training_id}.
        $session->training = $this->training_model->find($session->training_id);

        //Construction de la vue.
        $this->load->view('partials/header', array('title' => 'S\'inscrire à une session'));
        $this->load->view('partials/navbar');
        $this->load->view('members/registrations/apply', array('session' => $session, 'csrf' => $csrf));
        $this->load->view('partials/footer');
    }

    /**
     * Stocke une ressource en base de données.
     * 
     * 
     */
    public function store($id = '') {
        //On vérifie que l'on veut bien accéder à la page par méthode POST.
        if ($this->input->method(TRUE) != 'POST') {
            show_error('Method Not Allowed', 405);
        }

        if (empty($id)) {
            show_error('Internal Server Error', 500);
        }

        if ($this->_validate_registration_forms()) {
            $date = new DateTime();

            //Données à insérer.
            $data = array(
                'domain_id' => $this->input->post('domain_id'),
                'training_id' => $this->input->post('training_id'),
                'participant_id' => $this->input->post('participant_id'),
                'session_id' => $id,
                'cheque' => $this->input->post('cheque'),
                'date' => $date->format('Y-m-d H:i:s'),
                'status' => 0
            );

            //Insertion des données du formulaire dans la base de donnéees.
            if ($this->registration_model->insert($data)) {
                $this->session->set_flashdata('success', 'Vous avez bien été inscrit à la formation.');
                redirect('sessions/index');
            }
        }

        $this->session->set_flashdata('errors', $this->form_validation->error_array());
        redirect(current_url());
    }

    /**
     * Valide les données issues du formulaire d'inscription à une session.
     * 
     * @return bool
     */
    private function _validate_registration_forms() {
        //Définition des règles de validation du formulaire.
        $config = array(
            array(
                'field' => 'cheque',
                'label' => 'cheque',
                'rules' => 'integer',
            ),
            array(
                'field' => 'domain_id',
                'label' => 'id de domaine',
                'rules' => 'required|integer',
                'errors' => array(
                    'required' => 'Veuillez saisir un %s.',
                ),
            ),
            array(
                'field' => 'training_id',
                'label' => 'id de formation',
                'rules' => 'required|integer',
                'errors' => array(
                    'required' => 'Veuillez saisir un %s.',
                ),
            ),
            array(
                'field' => 'session_id',
                'label' => 'id de session',
                'rules' => 'required|integer',
                'errors' => array(
                    'required' => 'Veuillez saisir un %s.',
                ),
            ),
            array(
                'field' => 'participant_id',
                'label' => 'id du participant',
                'rules' => 'required|integer',
                'errors' => array(
                    'required' => 'Veuillez saisir un %s.',
                ),
            ),
        );

        $this->form_validation->set_rules($config);

        //Validation du formulaire.
        return $this->form_validation->run();
    }

}

/* End of file Registrations.php */
/* Location: ./application/controllers/Registrations.php */