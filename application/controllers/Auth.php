<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends MY_Controller {

    public function __construct() {
        parent::__construct();

        //Chargement de la bibliothèque de validation de formulaire.
        $this->load->library('form_validation');

        //Chargement du helper de formulaires.
        $this->load->helper('form');
    }

    /**
     * Affiche la page de connexion.
     * 
     * @return void
     */
    public function index() {
        if (!is_file(APPPATH . '/views/partials/header.php') OR ! is_file(APPPATH . '/views/members/auth/login.php') OR ! is_file(APPPATH . '/views/partials/footer.php')) {
            show_404();
        }

        //Jeton de sécurité.
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        //Construction de la vue.
        $this->load->view('partials/header', array('title' => 'Connexion', 'css' => array(base_url('assets/css/auth.css'))));
        $this->load->view('members/auth/login', array('csrf' => $csrf));
        $this->load->view('partials/footer');
    }

    /**
     * Affiche la page d'inscription.
     * 
     * @return void
     */
    public function register() {
        if (!is_file(APPPATH . '/views/partials/header.php') OR ! is_file(APPPATH . '/views/members/auth/register.php') OR ! is_file(APPPATH . '/views/partials/footer.php')) {
            show_404();
        }

        //Jeton de sécurité.
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        //Construction de la vue.
        $this->load->view('partials/header', array('title' => 'Connexion', 'css' => array(base_url('assets/css/auth.css'))));
        $this->load->view('members/auth/register', array('csrf' => $csrf));
        $this->load->view('partials/footer');
    }

    /**
     * Gère la déconnexion d'un utilisateur.
     */
    public function logout() {
        //On déconnecte l'utilisateur.
        $this->participant_model->logout();

        //On le redirige à la page de connexion.
        redirect('auth/index');
    }

    /**
     * Gère le processus d'authentification.
     */
    public function authenticate() {
        //On vérifie que l'on veut bien accéder à la page par méthode POST.
        if ($this->input->method(TRUE) != 'POST') {
            show_error('Method Not Allowed', 405);
        }

        //Si les données envoyées par le formulaire sont valides.
        if ($this->_validate_login_form()) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            //Chargement du modèle Participant.
            $this->load->model('participant_model');

            //Vérification des identifiants.
            if ($this->participant_model->login($username, $password)) {
                redirect('domains/index');
            } else {
                $this->session->set_flashdata('errors', array('Identifiants incorrects.'));
                redirect('auth/index');
            }
        }

        //Redirection vers la page de connexion.
        $this->session->set_flashdata('errors', $this->form_validation->error_array());
        redirect('/');
    }

    /**
     * Gère le processus d'inscription.
     */
    public function perform_registration() {
        //On vérifie que l'on veut bien accéder à la page par méthode POST.
        if ($this->input->method(TRUE) != 'POST') {
            show_error('Method Not Allowed', 405);
        }

        //Si les données envoyées par le formulaire sont valides.
        if ($this->_validate_registration_form()) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $data = array(
                'last_name' => $this->input->post('last_name'),
                'first_name' => $this->input->post('first_name')
            );

            //Inscription de l'utilisateur.
            if ($this->participant_model->register($username, $password, $data)) {
                redirect('auth/index');
            }
        }

        //Redirection vers la page d'inscription.
        $this->session->set_flashdata('errors', $this->form_validation->error_array());
        redirect('auth/register');
    }

    /**
     * Valide les données issues du formulaire de connexion.
     * 
     * @return bool
     */
    private function _validate_login_form() {
        //Définition des règles de validation du formulaire.
        $config = array(
            array(
                'field' => 'username',
                'label' => 'nom d\'utilisateur',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Veuillez saisir un %s.',
                )
            ),
            array(
                'field' => 'password',
                'label' => 'mot de passe',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Veuillez saisir un %s.',
                ),
            ),
        );

        $this->form_validation->set_rules($config);

        //Validation du formulaire.
        return $this->form_validation->run();
    }

    /**
     * Valide les données issues du formulaire d'inscription.
     * 
     * @return bool
     */
    private function _validate_registration_form() {
        //Définition des règles de validation du formulaire.
        $config = array(
            array(
                'field' => 'username',
                'label' => 'nom d\'utilisateur',
                'rules' => 'required|is_unique[participant.username]',
                'errors' => array(
                    'required' => 'Veuillez saisir un %s.',
                )
            ),
            array(
                'field' => 'password',
                'label' => 'mot de passe',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Veuillez saisir un %s.',
                ),
            ),
            array(
                'field' => 'last_name',
                'label' => 'nom',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Veuillez saisir un %s.',
                ),
            ),
            array(
                'field' => 'first_name',
                'label' => 'prénom',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Veuillez saisir un %s.',
                ),
            ),
        );

        $this->form_validation->set_rules($config);

        //Validation du formulaire.
        return $this->form_validation->run();
    }

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */