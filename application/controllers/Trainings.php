<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Trainings extends MY_Controller {

    public function __construct() {
        parent::__construct();

        //Vérification que l'utilisateur est bien connecté.
        if (!$this->participant_model->is_logged_in()) {
            redirect('auth/index');
        }

        //Chargement de la bibliothèque de validation de formulaire.
        $this->load->library('form_validation');
        //Chargement du helper de formulaires.
        $this->load->helper('form');
        //Importation du modèle Training.
        $this->load->model('training_model');
    }

    /**
     * Affiche une liste de ressources.
     */
    public function index($page = 0) {
        if (!is_file(APPPATH . '/views/partials/header.php') OR ! is_file(APPPATH . '/views/partials/navbar.php') OR ! is_file(APPPATH . '/views/accountants/trainings/index.php')
                OR ! is_file(APPPATH . '/views/partials/footer.php')) {
            show_404();
        }

        //Jeton de sécurité.
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        //Chargement de la bibliothèque de pagination.
        $this->load->library('pagination');

        //Chargement du helper Text.
        $this->load->helper('text');

        //Importation du modèle Domain.
        $this->load->model('domain_model');

        //Configuration de la pagination.
        $config['base_url'] = site_url('trainings');
        $config['total_rows'] = $this->training_model->record_count();
        $config['per_page'] = 10;
        //Configuration de l'apparence des liens de la pagination.
        $config['full_tag_open'] = '<div class="row"><ul class="pagination text-right" role="menubar" aria-label="Pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="current">';
        $config['cur_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="arrow">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="arrow">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo;';
        $config['next_link'] = '&raquo;';

        $this->pagination->initialize($config);

        //Récupération de la liste de toutes les formations.
        $trainings = $this->training_model->limit($config['per_page'], $page)->find_all();

        //On récupère le domaine pour chaque formation.
        foreach ($trainings as $training) {
            $training->domain = $this->domain_model->find($training->domain_id);
        }

        //Construction de la vue.
        $this->load->view('partials/header', array('title' => 'Liste des formations'));
        $this->load->view('partials/navbar');
        $this->load->view('accountants/trainings/index', array('trainings' => $trainings, 'offset' => $page, 'csrf' => $csrf));
        $this->load->view('partials/footer');
    }

    /**
     * Affiche le formulaire permettant de créer une ressource.
     */
    public function create() {
        if (!is_file(APPPATH . '/views/partials/header.php') OR ! is_file(APPPATH . '/views/partials/navbar.php') OR ! is_file(APPPATH . '/views/accountants/trainings/create.php')
                OR ! is_file(APPPATH . '/views/partials/footer.php')) {
            show_404();
        }

        //Importation du modèle Domain.
        $this->load->model('domain_model');

        //Récupération de la liste de tous les domaines (pour la liste déroulante).
        $domains = $this->domain_model->find_all();

        //Jeton de sécurité.
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        //Construction de la vue.
        $this->load->view('partials/header', array('title' => 'Ajouter une formation'));
        $this->load->view('partials/navbar');
        $this->load->view('accountants/trainings/create', array('csrf' => $csrf, 'domains' => $domains));
        $this->load->view('partials/footer');
    }

    /**
     * Stocke une ressource en base de données.
     */
    public function store() {
        //On vérifie que l'on veut bien accéder à la page par méthode POST.
        if ($this->input->method(TRUE) != 'POST') {
            show_error('Method Not Allowed', 405);
        }

        if ($this->_validate_training_forms()) {
            //Données à insérer.
            $data = array(
                'domain_id' => $this->input->post('domain_id'),
                'description' => $this->input->post('description'),
                'place' => $this->input->post('place'),
                'speaker' => $this->input->post('speaker'),
                'objectives' => $this->input->post('objectives'),
                'audience' => $this->input->post('audience'),
                'scheduling' => $this->input->post('scheduling'),
                'cost' => $this->input->post('cost')
            );

            //Insertion des données du formulaire dans la base de donnéees.
            if ($this->training_model->insert($data)) {
                $this->session->set_flashdata('success', 'La formation a bien été créée.');
                redirect('trainings/create');
            }
        }

        $this->session->set_flashdata('errors', $this->form_validation->error_array());
        redirect('trainings/create');
    }

    /**
     * Affiche la ressource specifiée.
     * 
     * @param int $id Id de la ressource.
     */
    public function show($id = '', $page = 0) {
        if (!is_file(APPPATH . '/views/partials/header.php') OR ! is_file(APPPATH . '/views/partials/navbar.php')
                OR ! is_file(APPPATH . '/views/accountants/trainings/show.php') OR ! is_file(APPPATH . '/views/partials/footer.php') OR empty($id) OR ! ( $training = $this->training_model->find($id))) {
            show_404();
        }

        //Jeton de sécurité.
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        //Importation du modèle Session.
        $this->load->model('session_model');

        //Chargement de la bibliothèque de pagination.
        $this->load->library('pagination');

        //Configuration de la pagination.
        $config['base_url'] = site_url('trainings/show/' . $id);
        $config['total_rows'] = $this->session_model->record_count();
        $config['per_page'] = 5;
        //Configuration de l'apparence des liens de la pagination.
        $config['full_tag_open'] = '<div class="row"><ul class="pagination text-right" role="menubar" aria-label="Pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="current">';
        $config['cur_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="arrow">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="arrow">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo;';
        $config['next_link'] = '&raquo;';

        $this->pagination->initialize($config);

        //SELECT * FROM session WHERE training_id = {$training->id}.
        $training->sessions = $this->session_model->where('training_id', $training->id)->limit($config['per_page'], $page)->find_all();

        //Construction de la vue.
        $this->load->view('partials/header', array('title' => 'Afficher une formation'));
        $this->load->view('partials/navbar');
        $this->load->view('accountants/trainings/show', array('training' => $training, 'offset' => $page, 'csrf' => $csrf));
        $this->load->view('partials/footer');
    }

    /**
     * Affiche le formulaire permettant de modifier une ressource.
     * 
     * @param int $id Id de la ressource.
     */
    public function edit($id = '') {
        if (!is_file(APPPATH . '/views/partials/header.php') OR ! is_file(APPPATH . '/views/partials/navbar.php') OR ! is_file(APPPATH . '/views/accountants/trainings/edit.php')
                OR ! is_file(APPPATH . '/views/partials/footer.php') OR empty($id) OR ! ( $training = $this->training_model->find($id))) {
            show_404();
        }

        //Jeton de sécurité.
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        //Importation du modèle Domain.
        $this->load->model('domain_model');

        //Récupération de la liste de tous les domaines (pour la liste déroulante).
        $domains = $this->domain_model->find_all();

        //Construction de la vue.
        $this->load->view('partials/header', array('title' => 'Modifier une formation'));
        $this->load->view('partials/navbar');
        $this->load->view('accountants/trainings/edit', array('training' => $training, 'csrf' => $csrf, 'domains' => $domains));
        $this->load->view('partials/footer');
    }

    /**
     * Met à jour la ressource spécifiée dans la base de données.
     * 
     * @param int $id Id de la ressource.
     */
    public function update($id) {
        //On vérifie que l'on veut bien accéder à la page par méthode POST.
        if ($this->input->method(TRUE) != 'POST') {
            show_error('Method Not Allowed', 405);
        }

        if ($this->_validate_training_forms()) {
            //Définition du tableau de données.
            $data = array(
                'domain_id' => $this->input->post('domain_id'),
                'description' => $this->input->post('description'),
                'place' => $this->input->post('place'),
                'speaker' => $this->input->post('speaker'),
                'objectives' => $this->input->post('objectives'),
                'audience' => $this->input->post('audience'),
                'scheduling' => $this->input->post('scheduling'),
                'cost' => $this->input->post('cost')
            );

            //Modification de la formation
            if ($this->training_model->update($id, $data)) {
                $this->session->set_flashdata('success', 'Formation modifiée avec succès.');
                redirect('trainings/edit/' . $id);
            }
        }

        show_error('Internal Server Error', 500);
    }

    /**
     * Supprime la ressource spécifiée de la base de données.
     * 
     * @param int $id Id de la ressource.
     */
    public function destroy($id) {
        //On vérifie que l'on veut bien accéder à la page par méthode POST.
        if ($this->input->method(TRUE) != 'POST') {
            show_error('Method Not Allowed', 405);
        }

        //Suppression de la formation.
        if ($this->training_model->delete($id)) {
            $this->session->set_flashdata('success', 'Formation supprimée avec succès.');
            redirect(current_url());
        }


        show_error('Internal Server Error', 500);
    }

    /**
     * Valide les données issues du formulaire de création de formation.
     * 
     * @return bool
     */
    private function _validate_training_forms() {
        //Définition des règles de validation du formulaire.
        $config = array(
            array(
                'field' => 'domain_id',
                'label' => 'domaine',
                'rules' => 'required|integer',
                'errors' => array(
                    'required' => 'Veuillez sélectionner un %s.',
                )
            ),
            array(
                'field' => 'description',
                'label' => 'description',
                'rules' => 'required|max_length[1000]',
                'errors' => array(
                    'required' => 'Veuillez saisir une %s.',
                ),
            ),
            array(
                'field' => 'place',
                'label' => 'lieu',
                'rules' => 'required|max_length[128]',
                'errors' => array(
                    'required' => 'Veuillez saisir un %s.',
                ),
            ),
            array(
                'field' => 'speaker',
                'label' => 'intervenant',
                'rules' => 'required|max_length[128]',
                'errors' => array(
                    'required' => 'Veuillez saisir un %s.',
                ),
            ),
            array(
                'field' => 'objectives',
                'label' => 'objectifs',
                'rules' => 'required|max_length[1000]',
                'errors' => array(
                    'required' => 'Veuillez saisir des %s.',
                ),
            ),
            array(
                'field' => 'audience',
                'label' => 'public',
                'rules' => 'required|max_length[128]',
                'errors' => array(
                    'required' => 'Veuillez saisir un %s.',
                ),
            ),
            array(
                'field' => 'scheduling',
                'label' => 'organisation',
                'rules' => 'required|max_length[255]',
                'errors' => array(
                    'required' => 'Veuillez saisir une %s.',
                ),
            ),
            array(
                'field' => 'cost',
                'label' => 'coût',
                'rules' => 'required|numeric',
                'errors' => array(
                    'required' => 'Veuillez saisir un %s.',
                ),
            ),
        );

        $this->form_validation->set_rules($config);

        //Validation du formulaire.
        return $this->form_validation->run();
    }

}

/* End of file Training.php */
/* Location: ./application/controllers/Training.php */