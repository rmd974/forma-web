<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sessions extends MY_Controller {

    public function __construct() {
        parent::__construct();

        //Vérification que l'utilisateur est bien connecté.
        if (!$this->participant_model->is_logged_in()) {
            redirect('auth/index');
        }

        //Chargement de la bibliothèque de validation de formulaire.
        $this->load->library('form_validation');
        //Chargement du helper de formulaires.
        $this->load->helper('form');
        //Importation du modèle Session.
        $this->load->model('session_model');
    }

    /**
     * Affiche une liste de ressources.
     */
    public function index($page = 0) {
        if (!is_file(APPPATH . '/views/partials/header.php') OR ! is_file(APPPATH . '/views/partials/navbar.php') OR ! is_file(APPPATH . '/views/accountants/sessions/index.php')
                OR ! is_file(APPPATH . '/views/partials/footer.php')) {
            show_404();
        }

        //Jeton de sécurité.
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        //Chargement de la bibliothèque de pagination.
        $this->load->library('pagination');

        //Importation du modèle Domain.
        $this->load->model('domain_model');
        //Importation du modèle Training.
        $this->load->model('training_model');

        //Configuration de la pagination.
        $config['base_url'] = site_url('sessions');
        $config['total_rows'] = $this->session_model->record_count();
        $config['per_page'] = 10;
        //Configuration de l'apparence des liens de la pagination.
        $config['full_tag_open'] = '<div class="row"><ul class="pagination text-right" role="menubar" aria-label="Pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="current">';
        $config['cur_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="arrow">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="arrow">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo;';
        $config['next_link'] = '&raquo;';

        $this->pagination->initialize($config);

        //Récupération de la liste de toutes les formations.
        $sessions = $this->session_model->limit($config['per_page'], $page)->find_all();

        //On récupère le domaine et la formation pour chaque session.
        foreach ($sessions as $session) {
            $session->domain = $this->domain_model->find($session->domain_id);
            $session->training = $this->training_model->find($session->training_id);
        }

        //Construction de la vue.
        $this->load->view('partials/header', array('title' => 'Liste des sessions'));
        $this->load->view('partials/navbar');
        $this->load->view('accountants/sessions/index', array('sessions' => $sessions, 'offset' => $page, 'csrf' => $csrf));
        $this->load->view('partials/footer');
    }

    /**
     * Affiche le formulaire permettant de créer une ressource.
     */
    public function create() {
        if (!is_file(APPPATH . '/views/partials/header.php') OR ! is_file(APPPATH . '/views/partials/navbar.php') OR ! is_file(APPPATH . '/views/accountants/sessions/create.php')
                OR ! is_file(APPPATH . '/views/partials/footer.php')) {
            show_404();
        }

        //Jeton de sécurité.
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        //Importation du modèle Training.
        $this->load->model('training_model');

        //Récupération de la liste de tous les formations (pour la liste déroulante).
        $trainings = $this->training_model->find_all();

        //Construction de la vue.
        $this->load->view('partials/header', array('title' => 'Ajouter une session'));
        $this->load->view('partials/navbar');
        $this->load->view('accountants/sessions/create', array('csrf' => $csrf, 'trainings' => $trainings));
        $this->load->view('partials/footer');
    }

    /**
     * Stocke une ressource en base de données.
     */
    public function store() {
        //On vérifie que l'on veut bien accéder à la page par méthode POST.
        if ($this->input->method(TRUE) != 'POST') {
            show_error('Method Not Allowed', 405);
        }

        //Importation du modèle Training.
        $this->load->model('training_model');

        //Si les données envoyées par le formulaire sont valides.
        if ($this->_validate_session_forms()) {
            //Définition du tableau de données.
            $data = array(
                'domain_id' => $this->training_model->find($this->input->post('training_id'))->domain_id,
                'training_id' => $this->input->post('training_id'),
                'start_date' => $this->input->post('start_date'),
                'end_date' => $this->input->post('end_date'),
                'status' => $this->input->post('status'),
                'description' => $this->input->post('description'),
                'available_seats' => $this->input->post('available_seats'),
            );

            //Insertion de la session.
            if ($this->session_model->insert($data)) {
                $this->session->set_flashdata('success', 'Session créée avec succès.');
                redirect('sessions/index');
            }
        }

        show_error('Internal Server Error', 500);
    }

    /**
     * Affiche la ressource specifiée.
     * 
     * @param int $id Id de la ressource.
     */
    public function show($id = '', $page = 0) {
        if (!is_file(APPPATH . '/views/partials/header.php') OR ! is_file(APPPATH . '/views/partials/navbar.php')
                OR ! is_file(APPPATH . '/views/accountants/sessions/show.php') OR ! is_file(APPPATH . '/views/partials/footer.php') OR empty($id) OR ! ( $session = $this->session_model->find($id))) {
            show_404();
        }

        //Jeton de sécurité.
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        //Importation du modèle Domain.
        $this->load->model('domain_model');
        //Importation du modèle Training.
        $this->load->model('training_model');

        //SELECT * FROM domain WHERE id = {$session->domain_id}.
        $session->domain = $this->domain_model->find($session->domain_id);

        //SELECT * FROM training WHERE id = {$session->training_id}.
        $session->training = $this->training_model->find($session->training_id);

        //Construction de la vue.
        $this->load->view('partials/header', array('title' => 'Afficher une session'));
        $this->load->view('partials/navbar');
        $this->load->view('accountants/sessions/show', array('session' => $session, 'offset' => $page, 'csrf' => $csrf));
        $this->load->view('partials/footer');
    }

    /**
     * Affiche le formulaire permettant de modifier une ressource.
     * 
     * @param int $id Id de la ressource.
     */
    public function edit($id = '') {
        
    }

    /**
     * Met à jour la ressource spécifiée dans la base de données.
     * 
     * @param int $id Id de la ressource.
     */
    public function update($id) {
        
    }

    /**
     * Supprime la ressource spécifiée de la base de données.
     * 
     * @param int $id Id de la ressource.
     */
    public function destroy($id) {
        //On vérifie que l'on veut bien accéder à la page par méthode POST.
        if ($this->input->method(TRUE) != 'POST') {
            show_error('Method Not Allowed', 405);
        }

        //Suppression du domaine.
        if ($this->session_model->delete($id)) {
            $this->session->set_flashdata('success', 'Session supprimée avec succès.');
            redirect('sessions/index');
        }

        show_error('Internal Server Error', 500);
    }

    /**
     * Valide les données issues du formulaire de création de formation.
     * 
     * @return bool
     */
    private function _validate_session_forms() {
        //Définition des règles de validation du formulaire.
        $config = array(
            array(
                'field' => 'training_id',
                'label' => 'formation',
                'rules' => 'required|integer',
                'errors' => array(
                    'required' => 'Veuillez sélectionner un %s.',
                )
            ),
            array(
                'field' => 'start_date',
                'label' => 'date de début',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Veuillez saisir une %s.',
                ),
            ),
            array(
                'field' => 'end_date',
                'label' => 'date de fin',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Veuillez saisir une %s.',
                ),
            ),
            array(
                'field' => 'description',
                'label' => 'description',
                'rules' => 'required|max_length[1000]',
                'errors' => array(
                    'required' => 'Veuillez saisir une %s.',
                ),
            ),
            array(
                'field' => 'available_seats',
                'label' => 'places disponibles',
                'rules' => 'required|integer',
                'errors' => array(
                    'required' => 'Veuillez saisir le nombre de %s.',
                ),
            )
        );

        $this->form_validation->set_rules($config);

        //Validation du formulaire.
        return $this->form_validation->run();
    }

}

/* End of file Session.php */
/* Location: ./application/controllers/Session.php */