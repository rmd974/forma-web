<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Domains extends MY_Controller {

    function __construct() {
        parent::__construct();

        //Vérification que l'utilisateur est bien connecté.
        if (!$this->participant_model->is_logged_in()) {
            redirect('auth/index');
        }

        //Importation du modèle Domain.
        $this->load->model('domain_model');
        //Chargement de la bibliothèque de validation de formulaire.
        $this->load->library('form_validation');
        //Chargement du helper de formulaires.
        $this->load->helper('form');
    }

    /**
     * Affiche une liste de ressources.
     */
    public function index($page = 0) {
        if (!is_file(APPPATH . '/views/partials/header.php') OR ! is_file(APPPATH . '/views/partials/navbar.php') OR ! is_file(APPPATH . '/views/accountants/domains/index.php')
                OR ! is_file(APPPATH . '/views/partials/footer.php')) {
            show_404();
        }

        //Chargement de la bibliothèque de pagination.
        $this->load->library('pagination');

        //Configuration de la pagination.
        $config['base_url'] = site_url('domains');
        $config['total_rows'] = $this->domain_model->record_count();
        $config['per_page'] = 10;
        //Configuration de l'apparence des liens de la pagination.
        $config['full_tag_open'] = '<div class="row"><ul class="pagination text-right" role="menubar" aria-label="Pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="current">';
        $config['cur_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="arrow">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="arrow">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo;';
        $config['next_link'] = '&raquo;';

        $this->pagination->initialize($config);

        //Récupération de la liste de tous les domaines.
        $domains = $this->domain_model->limit($config['per_page'], $page)->find_all();

        //Jeton de sécurité.
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        //Construction de la vue.
        $this->load->view('partials/header', array('title' => 'Liste des domaines'));
        $this->load->view('partials/navbar');
        $this->load->view('accountants/domains/index', array('domains' => $domains, 'offset' => $page, 'csrf' => $csrf));
        $this->load->view('partials/footer');
    }

    /**
     * Affiche le formulaire permettant de créer une ressource.
     */
    public function create() {
        if (!is_file(APPPATH . '/views/partials/header.php') OR ! is_file(APPPATH . '/views/partials/navbar.php') OR ! is_file(APPPATH . '/views/accountants/domains/create.php') OR ! is_file(APPPATH . '/views/partials/footer.php')) {
            show_404();
        }

        //Vérification que l'utilisateur est bien administrateur.
        if (!$this->participant_model->is_admin()) {
            show_error('Forbidden', 403);
        }

        //Jeton de sécurité.
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        //Construction de la vue.
        $this->load->view('partials/header', array('title' => 'Ajouter un domaine'));
        $this->load->view('partials/navbar');
        $this->load->view('accountants/domains/create', array('csrf' => $csrf));
        $this->load->view('partials/footer');
    }

    /**
     * Stocke une ressource en base de données.
     * 
     * @param int $id Id de la ressource.
     */
    public function store() {
        //On vérifie que l'on veut bien accéder à la page par méthode POST.
        if ($this->input->method(TRUE) != 'POST') {
            show_error('Method Not Allowed', 405);
        }

        //Vérification que l'utilisateur est bien administrateur.
        if (!$this->participant_model->is_admin()) {
            show_error('Forbidden', 403);
        }

        //Si les données envoyées par le formulaire sont valides.
        if ($this->_validate_domain_forms()) {
            //Définition du tableau de données.
            $data = array(
                'name' => $this->input->post('name'),
            );

            //Insertion du domaine.
            if ($this->domain_model->insert($data)) {
                $this->session->set_flashdata('success', 'Domaine créé avec succès.');
                redirect('domains/index');
            }
        }

        show_error('Internal Server Error', 500);
    }

    /**
     * Affiche la ressource specifiée.
     * 
     * @param int $id Id de la ressource.
     */
    public function show($id = '', $page = 0) {
        if (!is_file(APPPATH . '/views/partials/header.php') OR ! is_file(APPPATH . '/views/partials/navbar.php')
                OR ! is_file(APPPATH . '/views/accountants/domains/show.php') OR ! is_file(APPPATH . '/views/partials/footer.php') OR empty($id) OR ! ( $domain = $this->domain_model->find($id))) {
            show_404();
        }

        //Jeton de sécurité.
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        //Chargement du helper Text.
        $this->load->helper('text');

        //Importation du modèle Training.
        $this->load->model('training_model');

        //Chargement de la bibliothèque de pagination.
        $this->load->library('pagination');

        //Configuration de la pagination.
        $config['base_url'] = site_url('domains/show/' . $id);
        $config['total_rows'] = $this->training_model->record_count();
        $config['per_page'] = 5;
        //Configuration de l'apparence des liens de la pagination.
        $config['full_tag_open'] = '<div class="row"><ul class="pagination text-right" role="menubar" aria-label="Pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="current">';
        $config['cur_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="arrow">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="arrow">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo;';
        $config['next_link'] = '&raquo;';

        $this->pagination->initialize($config);

        //SELECT * FROM training WHERE domain_id = {$domain->id}.
        $domain->trainings = $this->training_model->where('domain_id', $domain->id)->limit($config['per_page'], $page)->find_all();

        //Construction de la vue.
        $this->load->view('partials/header', array('title' => 'Afficher un domaine'));
        $this->load->view('partials/navbar');
        $this->load->view('accountants/domains/show', array('domain' => $domain, 'offset' => $page, 'csrf' => $csrf));
        $this->load->view('partials/footer');
    }

    /**
     * Affiche le formulaire permettant de modifier une ressource.
     * 
     * @param int $id Id de la ressource.
     */
    public function edit($id = '') {
        if (!is_file(APPPATH . '/views/partials/header.php') OR ! is_file(APPPATH . '/views/partials/navbar.php')
                OR ! is_file(APPPATH . '/views/accountants/domains/edit.php') OR ! is_file(APPPATH . '/views/partials/footer.php') OR empty($id) OR ! ($domain = $this->domain_model->find($id))) {
            show_404();
        }

        //Vérification que l'utilisateur est bien administrateur.
        if (!$this->participant_model->is_admin()) {
            show_error('Forbidden', 403);
        }

        //Jeton de sécurité.
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        //Construction de la vue.
        $this->load->view('partials/header', array('title' => 'Modifier un domaine'));
        $this->load->view('partials/navbar');
        $this->load->view('accountants/domains/edit', array('csrf' => $csrf, 'domain' => $domain));
        $this->load->view('partials/footer');
    }

    /**
     * Met à jour la ressource spécifiée dans la base de données.
     * 
     * @param int $id Id de la ressource.
     */
    public function update($id) {
        //On vérifie que l'on veut bien accéder à la page par méthode POST.
        if ($this->input->method(TRUE) != 'POST') {
            show_error('Method Not Allowed', 405);
        }

        //Vérification que l'utilisateur est bien administrateur.
        if (!$this->participant_model->is_admin()) {
            show_error('Forbidden', 403);
        }

        if ($this->_validate_domain_forms()) {
            //Définition du tableau de données.
            $data = array(
                'name' => $this->input->post('name')
            );

            //Modification du domaine.
            if ($this->domain_model->update($id, $data)) {
                $this->session->set_flashdata('success', 'Domaine modifié avec succès.');
                redirect('domains/edit/' . $id);
            }
        }

        //Redirection vers la page de connexion.
        $this->session->set_flashdata('errors', $this->form_validation->error_array());
        redirect('domains/edit/' . $id);
    }

    /**
     * Supprime la ressource spécifiée de la base de données.
     * 
     * @param int $id Id de la ressource.
     */
    public function destroy($id) {
        //On vérifie que l'on veut bien accéder à la page par méthode POST.
        if ($this->input->method(TRUE) != 'POST') {
            show_error('Method Not Allowed', 405);
        }

        //Vérification que l'utilisateur est bien administrateur.
        if (!$this->participant_model->is_admin()) {
            show_error('Forbidden', 403);
        }

        //Suppression du domaine.
        if ($this->domain_model->delete($id)) {
            $this->session->set_flashdata('success', 'Domaine supprimé avec succès.');
            redirect('domains/index');
        }

        show_error('Internal Server Error', 500);
    }

    /**
     * Valide les données issues des formulaires concernant les domaines.
     * 
     * @return bool
     */
    private function _validate_domain_forms() {
        //Définition des règles de validation du formulaire.
        $config = array(
            array(
                'field' => 'name',
                'label' => 'nom de domaine',
                'rules' => 'required|max_length[128]',
                'errors' => array(
                    'required' => 'Veuillez saisir un %s.',
                )
            )
        );

        $this->form_validation->set_rules($config);

        //Validation du formulaire.
        return $this->form_validation->run();
    }

}

/* End of file Domain.php */
/* Location: ./application/controllers/Domain.php */